from django.urls import path
from . import views


urlpatterns = [
    path('', views.GameList.as_view(), name='game_list'),
    path('game/create', views.GameCreate.as_view(), name='game_create'),
    path('game/update/<int:pk>', views.GameUpdate.as_view(), name='game_update'),
    path('game/<int:pk>', views.GameDetail.as_view(), name='game_detail'),
    path('game/<int:pk>', views.GameDelete.as_view(), name='game_delete'),
    path('game/card/create/<int:pk>', views.cardCreate, name='card_create'),
    path('game/card/<int:pk>', views.CardUpdate.as_view(), name='card_update'),
    path('login/', views.loginView, name='login_view'),
    path('logout/', views.logoutView, name='logout_view'),
]