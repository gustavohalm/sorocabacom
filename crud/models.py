from django.db import models


class Game(models.Model):
    name = models.CharField(max_length=256)
    background_image = models.ImageField(upload_to='images/')
    spotlight_image = models.ImageField(upload_to='images/')
    form_description = models.CharField(max_length=1028)
    description_phrase = models.CharField(max_length=512)


class Character(models.Model):
    game = models.ForeignKey('crud.Game', on_delete=models.CASCADE, related_name='cards')
    image = models.ImageField(upload_to='images/')
    text = models.CharField(max_length=256)
