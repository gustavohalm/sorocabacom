from django import forms
from . import models


class GameForm(forms.ModelForm):
    class Meta:
        model = models.Game
        fields = '__all__'

        widgets = {
            'name': forms.TextInput(attrs= {'class': 'form-control'}),
            'background_image': forms.FileInput(attrs= {'class': 'form-control'}),
            'spotlight_image': forms.FileInput(attrs= {'class': 'form-control'}),
            'form_description': forms.TextInput(attrs= {'class': 'form-control'}),
            'description_phrase': forms.TextInput(attrs= {'class': 'form-control'})
        }


class CharacterForm(forms.ModelForm):
    class Meta:
        model = models.Character
        exclude = ['game',]

        widgets = {
            'image': forms.FileInput(attrs= {'class': 'form-control'}),
            'text': forms.TextInput(attrs= {'class': 'form-control'})
        }


class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(attrs=dict(required=True, max_length=30, placeholder='user')), label='Usuário')
    password = forms.CharField(
        widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, placeholder='********')), label='Sua Senha')