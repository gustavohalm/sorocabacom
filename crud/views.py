from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.views.generic import ( TemplateView, ListView, DetailView,CreateView, UpdateView, DeleteView)
from django.contrib.auth.mixins import LoginRequiredMixin
from . import forms, models
# Create your views here.1


def loginView( request):
    if request.method == 'POST':
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            print('yea')
            return HttpResponseRedirect(reverse('game_list'))
        else:
            form = forms.LoginForm
    else:
        form = forms.LoginForm
    context = {'form': form, }
    return render(request, 'crud/login_view.html', context)


def logoutView(request):
    pass


class GameCreate(LoginRequiredMixin,CreateView):
    model = models.Game
    login_url = '/login'

    form_class =  forms.GameForm
    success_url = '../'
    template_name =  'crud/game_create.html'


class GameList(LoginRequiredMixin,ListView):
    template_name = 'crud/game_list.html'
    login_url = '/painel/login'
    def get_queryset(self):
        return models.Game.objects.all()


class GameDetail(LoginRequiredMixin,DetailView):
    model = models.Game
    login_url = '/login'

    template_name = 'crud/game_detail.html'


class GameUpdate(LoginRequiredMixin,UpdateView):
    model = models.Game
    login_url = '/login'

    form_class = forms.GameForm
    template_name = 'crud/game_update.html'


class GameDelete(LoginRequiredMixin,DeleteView):
    model = models.Game
    login_url = '/login'

    template_name = 'crud/game-delete.html'
    success_url = '../../'


def cardCreate(request, pk):
    game = get_object_or_404(models.Game, id=pk)

    if request.method == 'POST':
        form = forms.CharacterForm(request.POST,request.FILES)
        print('ok')
        if form.is_valid():
            card = form.save(commit=False)
            card.game = game
            card.save()
            print('ok')
    url = '../../' + str(pk)
    return redirect(url)


class CardUpdate(LoginRequiredMixin,UpdateView):
    model = models.Character
    form_class = forms.CharacterForm
    template_name = 'crud/card_update.html'


