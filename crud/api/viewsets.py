from django.shortcuts import get_object_or_404

from . import serializers
from rest_framework.viewsets import  ModelViewSet
from rest_framework.permissions import  IsAuthenticatedOrReadOnly
from crud import models


class GameViewSet(ModelViewSet):
    serializer_class = serializers.GameSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = models.Game.objects.all()


class CharacterViewSet(ModelViewSet):
    serializer_class = serializers.CharacterSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        if 'game' in self.request.GET:
            game_id = self.request.GET['game']
            game = get_object_or_404(models.Game, id=game_id)
            return models.Character.objects.filter(game=game)
        return models.Character.objects.all()